package docs

import ucontroller "gitlab.com/antoxa2614/parser_user_microservices/user-servise/internal/modules/user/controller"

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
//   -Bearer: []
// responses:
//   200: profileResponse

// swagger:parameters profileResponse
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}

package storages

import (
	"gitlab.com/antoxa2614/parser_user_microservices/user-servise/internal/db/adapter"
	"gitlab.com/antoxa2614/parser_user_microservices/user-servise/internal/infrastructure/cache"
	vstorage "gitlab.com/antoxa2614/parser_user_microservices/user-servise/internal/modules/auth/storage"
	ustorage "gitlab.com/antoxa2614/parser_user_microservices/user-servise/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}

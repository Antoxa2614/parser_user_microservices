package main

import "gitlab.com/antoxa2614/parser_user_microservices/go-parser/model"

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /auth/register auth registerRequest
// Регистрация пользователя.
// responses:
//   200: registerResponse

// swagger:parameters registerRequest
type registerRequest struct {
	// in:body
	Body model.RegisterRequest
}

// swagger:response registerResponse
type registerResponse struct {
	// in:body
	Body model.RegisterResponse
}

// swagger:route POST /auth/login auth loginRequest
// Авторизация пользователя.
// responses:
//   200: loginResponse

// swagger:parameters loginRequest
type loginRequest struct {
	// in:body
	Body model.LoginRequest
}

// swagger:response loginResponse
type loginResponse struct {
	// in:body
	Body model.LoginResponse
}

// swagger:route POST /auth/changepassword auth ChangePasswordRequest
// Смена пароля.
// security:
//   - Bearer: []
// responses:

//   200: ChangePasswordResponse

// swagger:parameters ChangePasswordRequest
type ChangePasswordRequest struct {
	// in:body
	Body model.ChangePasswordRequest
}

// swagger:response ChangePasswordResponse
type ChangePasswordResponse struct {
	// in:body
	Body model.ChangePasswordResponse
}

// swagger:route POST /search vacancy vacancySearchRequest
// Создание вакансии.
// security:
//   - Bearer: []

// responses:
//   200: vacancySearchResponse

// swagger:parameters vacancySearchRequest
type vacancySearchRequest struct {
	// in:body
	Body model.Vacancy
}

// swagger:response vacancySearchResponse
type vacancySearchResponse struct {
	// in:body
	Body model.Vacancy
}

// swagger:route POST /get vacancy vacancyIdGetRequest
// Поиск вакансии по ID.
// security:
//   - Bearer: []

// responses:
//   200: vacancyIdGetResponse

// swagger:parameters vacancyIdGetRequest
type vacancyIdGetRequest struct {
	// in:body
	VacancyId string `json:"vacancyId"`
}

// swagger:response vacancyIdGetResponse
type vacancyIdGetResponse struct {
	// in:body
	Body model.Vacancy
}

// swagger:route DELETE /delete vacancy vacancyDeleteRequest
// Удалить вакансию.
// security:
//   - Bearer: []

// responses:
//   200: vacancyDeleteResponse

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// in:body
	VacancyId string `json:"vacancyId"`
}

// swagger:response vacancyDeleteResponse
type vacancyDeleteResponse struct {
	// in:body
	Body model.Vacancy
}

// swagger:route GET /list vacancy vacancyListRequest
// Список всех вакансий.
// security:
//   - Bearer: []

// responses:
//   200: vacancyListResponse

// swagger:parameters vacancyListRequest

// swagger:response vacancyListResponse
type vacancyListResponse struct {
	// in:body
	Body []model.Vacancy
}

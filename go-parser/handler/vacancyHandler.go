package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/model"
	"net/http"
	"strconv"
)

func (v *VakancerHandler) CreateVacancy(ctx *gin.Context) {
	var vacan model.Vacancy
	if err := ctx.BindJSON(&vacan); err != nil {
		newErrorResponse(ctx, http.StatusBadRequest, err.Error())
		return
	}

	err := v.servise.Create(vacan)
	if err != nil {
		newErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}

	ctx.JSON(http.StatusCreated, vacan)
}

func (v *VakancerHandler) GetVacancyById(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		newErrorResponse(ctx, http.StatusBadRequest, "invalid id param")
		return
	}

	vacancy, err := v.servise.GetBiId(id)
	if err != nil {
		newErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, vacancy)
}

func (v *VakancerHandler) DeleteVacancy(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		newErrorResponse(ctx, http.StatusBadRequest, "invalid id param")
		return
	}

	err = v.servise.Delete(id)
	if err != nil {
		newErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, statusResponse{
		Status: "ok",
	})
}

func (v *VakancerHandler) GetListVacancy(ctx *gin.Context) {
	vacancies, err := v.servise.GetList()
	if err != nil {
		newErrorResponse(ctx, http.StatusInternalServerError, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, getAllVacanciesResponse{
		Data: vacancies,
	})
}

type getAllVacanciesResponse struct {
	Data []model.Vacancy `json:"data"`
}

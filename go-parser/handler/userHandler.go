package handler

import (
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/model"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h *VakancerHandler) Login(c *gin.Context) {
	var reqBody struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	token, err := h.servise.Login(reqBody.Email, reqBody.Password)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"Authorization": token.Data})
}

func (h *VakancerHandler) Register(c *gin.Context) {
	var reqBody struct {
		Email          string `json:"email"`
		Password       string `json:"password"`
		RetypePassword string `json:"retype_password"`
	}
	if err := c.ShouldBindJSON(&reqBody); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Println("not error")

	res, err := h.servise.Register(reqBody.Email, reqBody.Password, reqBody.RetypePassword)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"res": res})
}

func (h *VakancerHandler) ChangePassword(c *gin.Context) {
	var req model.ChangePasswordRequest
	user, _ := c.Get("user")
	email := user.(*model.User).Email

	if err := c.ShouldBindJSON(&req); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	res, err := h.servise.ChangePassword(email, req.OldPassword, req.NewPassword)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"res": res.Data})
}

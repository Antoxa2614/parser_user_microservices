package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/internal/middleware"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/internal/userclient"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/servise"
)

type VakancerHandler struct {
	servise        servise.Vacancer
	authMiddleware *middleware.AuthMiddleware
}
type Message struct {
	message string `json:"message"`
	code    int    `json:"code"`
}

func NewHandler(data servise.Vacancer, authServiceClient *userclient.UsersClient) VakancerHandler {
	return VakancerHandler{
		servise:        data,
		authMiddleware: middleware.NewAuthMiddleware(authServiceClient),
	}
}
func (h *VakancerHandler) Routes() *gin.Engine {
	router := gin.New()

	// Защищенные роуты
	authorized := router.Group("/")
	authorized.Use(h.authMiddleware.CheckStrict())
	{
		authorized.POST("/vacancy", h.CreateVacancy)
		authorized.GET("/vacs", h.GetListVacancy)
		authorized.GET("/vacancy/:id", h.GetVacancyById)
		authorized.DELETE("/vacancy/:id", h.DeleteVacancy)
	}

	// Открытые роуты
	router.GET("/swagger/", h.SwaggerUI)
	router.POST("/auth/login", h.Login)
	router.POST("/auth/register", h.Register)
	router.POST("/auth/changepassword", h.ChangePassword)

	return router
}

package servise

import (
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/model"
)

func (s *VacancerServise) Login(email, password string) (*model.LoginResponse, error) {
	return s.clientUs.Login(email, password)
}

func (s *VacancerServise) Register(email, password, retypePass string) (*model.RegisterResponse, error) {
	return s.clientUs.Register(email, password, retypePass)
}

func (s *VacancerServise) ChangePassword(email, oldPassword, newPassword string) (*model.ChangePasswordResponse, error) {
	return s.clientUs.ChangePassword(email, oldPassword, newPassword)
}

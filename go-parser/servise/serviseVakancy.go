package servise

import (
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/internal/userclient"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/model"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/repository"
	"golang.org/x/net/context"
	"log"
	"time"
)

type Vacancer interface {
	Create(vac model.Vacancy) error
	GetBiId(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
	Login(email, password string) (*model.LoginResponse, error)
	Register(email, password, retypePassword string) (*model.RegisterResponse, error)
	ChangePassword(email, oldPassword, newPassword string) (*model.ChangePasswordResponse, error)
}

type VacancerServise struct {
	repo     repository.StorageVakancer
	clientUs *userclient.UsersClient
	client   *redis.Client
	ctx      context.Context
}

func NewVacancerStorage(repo repository.StorageVakancer, client *userclient.UsersClient) Vacancer {
	return &VacancerServise{
		repo:     repo,
		clientUs: client,
		client: func() *redis.Client {
			client := redis.NewClient(&redis.Options{Addr: "localhost:6379"})
			defer client.Close()
			return client
		}(),
		ctx: context.Background(),
	}

}

func (s *VacancerServise) Create(vac model.Vacancy) error {
	return s.repo.Create(vac)
}

func (s *VacancerServise) GetBiId(id int) (model.Vacancy, error) {

	result, err := s.client.Get(s.ctx, fmt.Sprintf("vacancy:%d", id)).Result()
	if err != nil {
		log.Println("Добавление вакансии в базу Redis")
		vacancy, err := s.repo.GetBiId(id)
		if err != nil {
			return model.Vacancy{}, err
		}
		bytes, err := json.Marshal(vacancy)
		if err != nil {
			return model.Vacancy{}, err
		}
		err = s.client.Set(s.ctx, fmt.Sprintf("vacancy:%d", id), bytes, time.Hour*4).Err()
		if err != nil {
			return model.Vacancy{}, err
		}
		return s.repo.GetBiId(id)
	} else {
		var vacancy model.Vacancy
		err = json.Unmarshal([]byte(result), &vacancy)
		if err != nil {
			return model.Vacancy{}, err
		}

		return vacancy, nil
	}
}

func (s *VacancerServise) GetList() ([]model.Vacancy, error) {
	return s.repo.GetList()
}

func (s *VacancerServise) Delete(id int) error {
	s.client.Del(s.ctx, fmt.Sprintf("vacancy:%d", id))
	return s.repo.Delete(id)
}

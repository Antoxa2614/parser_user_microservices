package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/internal/userclient"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/model"

	"net/http"

	"github.com/gin-gonic/gin"
)

type AuthMiddleware struct {
	authServiceClient *userclient.UsersClient
}

func NewAuthMiddleware(authServiceClient *userclient.UsersClient) *AuthMiddleware {
	return &AuthMiddleware{
		authServiceClient: authServiceClient,
	}
}

func (am *AuthMiddleware) CheckStrict() gin.HandlerFunc {
	return func(c *gin.Context) {
		r := c.Request
		w := c.Writer
		token := r.Header.Get("Authorization")
		if token == "" {
			http.Error(w, "no authorization header provided", http.StatusUnauthorized)
			c.Abort()
			return
		}

		user, err := ValidateAccessToken(token) //token
		if err != nil {
			http.Error(w, "error during authorization check", http.StatusInternalServerError)
			c.Abort()
			return
		}

		if user == nil {
			fmt.Println(user)
			http.Error(w, "invalid access token", http.StatusUnauthorized)
			c.Abort()
			return
		}
		c.Set("user", user)

		c.Next()
	}
}

func ValidateAccessToken(token string) (*model.User, error) {
	user := model.User{}
	req, err := http.NewRequest("GET", "http://localhost:8090/api/1/auth/user", nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {

		err = json.NewDecoder(resp.Body).Decode(&user)
		if err != nil {
			return nil, err
		}

		return &user, nil
	}
	return nil, errors.New("failed to validate access token")
}

package userclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/model"
	"net/http"
)

type UsersClient struct {
	baseURL string
	client  *http.Client
}

func NewUsersClient(baseURL string) *UsersClient {
	return &UsersClient{
		baseURL: baseURL,
		client:  &http.Client{},
	}
}

func (s *UsersClient) Register(email, password, retypePassword string) (*model.RegisterResponse, error) {
	reqBody, err := json.Marshal(model.RegisterRequest{
		Email:          email,
		Password:       password,
		RetypePassword: retypePassword,
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/1/auth/register", s.baseURL), bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var resBody model.RegisterResponse
	err = json.NewDecoder(res.Body).Decode(&resBody)
	if err != nil {
		return nil, err
	}

	return &resBody, nil
}

func (s *UsersClient) Login(email, password string) (*model.LoginResponse, error) {
	// create the request body
	reqBody, err := json.Marshal(model.LoginRequest{
		Email:    email,
		Password: password,
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/1/auth/login", s.baseURL), bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var resBody model.LoginResponse
	err = json.NewDecoder(res.Body).Decode(&resBody)
	if err != nil {
		return nil, err
	}

	return &resBody, nil
}

func (s *UsersClient) ChangePassword(newPassword, oldPassword, email string) (*model.ChangePasswordResponse, error) {
	reqBody, err := json.Marshal(model.ChangePasswordRequest{
		OldPassword: oldPassword,
		NewPassword: newPassword,
		Email:       email,
	})
	if err != nil {
		return nil, err
	}

	// create the request
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/1/auth/change", s.baseURL), bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	// send the request
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// decode the response
	var resBody model.ChangePasswordResponse
	err = json.NewDecoder(res.Body).Decode(&resBody)
	if err != nil {
		return nil, err
	}

	return &resBody, nil
}

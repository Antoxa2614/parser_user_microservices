package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/db"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/handler"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/internal/userclient"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/selenium"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/servise"
	"golang.org/x/net/context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	port := ":8080"
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
	nameDatabase := os.Getenv("nameDatabase")
	data := db.NewDBController(nameDatabase)
	client := userclient.NewUsersClient("http://userservise:8090")
	vacanServise := servise.NewVacancerStorage(data, client)
	vacanHandler := handler.NewHandler(vacanServise, client)
	time.Sleep(5 * time.Second)
	go selenium.Parsing(vacanServise)
	VacancyRouter := vacanHandler.Routes()

	srv := http.Server{Addr: port, Handler: VacancyRouter}
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

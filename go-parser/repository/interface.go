package repository

import "gitlab.com/antoxa2614/parser_user_microservices/go-parser/model"

type StorageVakancer interface {
	Create(vac model.Vacancy) error
	GetBiId(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

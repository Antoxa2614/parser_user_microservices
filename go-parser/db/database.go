package db

import (
	"fmt"
	"gitlab.com/antoxa2614/parser_user_microservices/go-parser/repository"
)

type DBController struct { // Pet контроллер
	repository.StorageVakancer
}

func NewDBController(nameDatabase string) *DBController { // конструктор нашего контроллера

	switch nameDatabase {
	case "postgres":
		fmt.Println("Старт базы Postgre")
		return &DBController{repository.NewPostgres()}
	case "mongo":
		fmt.Println("Старт базы Mongo")
		return &DBController{repository.NewMongo()}
	default:
		fmt.Println("Старт базы на локальном сервере")
		return &DBController{repository.NewVacanStorage()}
	}

}
